# Eudtweg Connector

Deze repository bevat de projectcode voor de Eudtweg connector.

Als je vragen hebt, kan je contact opnemen met support, op www.purplepolarbear.nl

## Overige Eudtweg componenten
De Eudtweg templates kunnen gevonden worden op https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates

De Eudtweg evaluator kan gevonden worden op https://gitlab.com/fair-data-communities/eudtweg/eudtweg-evaluator

## Support
Voor meer informatie, kan je contact opnemen met Purple Polar Bear: www.purplepolarbear.nl

## Roadmap
De connector is onderdeel van de Eudtweg samenwerking.

## Contributie
Zie de contributing file: https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/blob/main/CONTRIBUTING

## Licentie
De licentie wordt op volgende locatie beschreven: https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/blob/main/LICENCE
