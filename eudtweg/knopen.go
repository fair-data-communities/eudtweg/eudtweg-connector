package eudtweg

import(
  "encoding/xml"
  "fmt"
  "strings"
  "strconv"

  // "purplepolarbear.nl/connector/pipeliner/models"
  "purplepolarbear.nl/connector/lib/gml"
)

//
// API
//

type Knoop struct {
  XMLName             xml.Name
  Id                  string `xml:"gml:id,attr"`

  ObjectId            string `xml:"Eudtweg:OBJECTID"`
  Name                string `xml:"Eudtweg:Name"`
  Provider            string `xml:"Eudtweg:Provider"`
  Objecttype          string `xml:"Eudtweg:Objecttype"`
  Shape               *gml.GmlShape `xml:"Eudtweg:Shape"`
  Puttype             string `xml:"Eudtweg:Puttype"`
  Materiaal           string `xml:"Eudtweg:Materiaal"`
  Afvoertype          string `xml:"Eudtweg:Afvoertype"`

  transientPolygonState       string
}

func NewKnoop(layer *KnopenLayer) *Knoop {
  return &Knoop{
    Provider: layer.Provider(),
    Shape: &gml.GmlShape{},
  }
}

func (asset *Knoop) XmlName() *xml.Name {
  return &asset.XMLName
}

func (asset *Knoop) SetValue(key string, value interface {}) {
  /*
  fmt.Printf("*************************************\n")
  fmt.Printf("************************************* %s\n", key)
  fmt.Printf("*************************************\n")
  */

  switch key {
  case "Id":
    asset.Id = value.(string)
  case "id":
    asset.ObjectId = value.(string)
  case "name":
    asset.Name = value.(string)
  case "objecttype":
    val := value.(string)

    mappings := map[string]string{
      "https://gitlab.com/fair-data-communities/sski/sski-templates/-/raw/master/definitions/asset_md.rdf#id-bbf7a8d1-6c5c-48c7-b37e-334ade35ad9c": "1",
    }
    newVal := mappings[val]
    if newVal != "" {
      val = newVal
    }

    asset.Objecttype = val
  case "puttype":
    val := value.(string)

    mappings := map[string]string{
      "https://gitlab.com/fair-data-communities/sski/sski-templates/-/raw/master/definitions/asset_md.rdf#id-bbf7a8d1-6c5c-48c7-b37e-334ade35ad9c": "1",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-9d90ab25-7d72-4f5a-9b63-9cfbdd494c14": "eindput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-8de0d491-175a-4c12-a8ae-7f77b59cc305": "inspectieput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-137a74c1-80c3-4f8d-bbfa-31601b484c8d": "fictieve put!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-7887f4ba-1a15-4591-b2ab-9c3e38c1b79c": "inlaat!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-0ab0c2b4-f7a9-4aad-825d-5c0fd0032e77": "externe overstort!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-af3e269d-35c4-4789-9f5b-049a434cbb6d": "stuwput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-5ce140e3-1ca5-4a13-a2af-cd0dc10ef676": "blinde put!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-fb469272-e1d9-4e98-8e25-a5123f7c3ad5": "onbekend!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-fbacc23c-1142-496a-96e6-c3cf35bacffb": "open water!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-bf3ae5fb-b798-4c58-ba97-bc10bddaca40": "pompput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-53cbe206-a956-4819-a06c-71a15437c838": "lanceerinrichting!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-d9761f24-9114-4a45-8a14-6366e08200a6": "interne overstort!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-3ebd938d-75fa-4946-b809-b5249a3266f1": "in-/uitstroomput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-ec2b1eaf-22e1-4051-a1a3-c38fd3e9a605": "uitstroombak!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-3612697a-e5a9-4944-9f8e-3efb72eaddc0": "duiker!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-c548095a-a303-4af8-8a0a-0dcca735f5bc": "gemaal!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-cfc83244-6f59-475d-a91a-eb53c71bb58a": "ontvangstput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-f32d2c08-b70d-4321-9726-f7a698c54d3b": "inspectie luik!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-6607aff7-ee9c-4e28-b8ea-ac777030222d": "ontstoppingstuk!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-756754a0-d726-4164-93cc-561c3bd96b1c": "drainput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-f5a2a81c-ac2d-4960-85a2-0a2dafdf473a": "randvoorziening!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-075ece8e-116f-4dd5-8722-17770c125d80": "infiltratieunit!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-d38779c7-628a-427c-a210-78caaf4a1c63": "schildmuur!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-ac3c0c79-cc91-4b49-8595-7ed3359430d8": "kruisingsput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-295c3589-08ee-4233-b2f2-5ff4dcad2510": "afsluiter!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-2a0b1611-b42b-4abd-b552-eb8a580f7158": "besturingskast!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-dba564b7-d82c-43e3-bc41-4c1d4d6a3718": "geknevelde putdeksel!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-2363a798-cc0b-43ed-b9b4-4826d62848df": "y-stuk beton!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-14375ab7-863f-43b8-8bf3-4fda78bcd4c5": "terugslagklepput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-73db3cfb-c0f2-4af2-baa2-fd15105a8e91": "ontluchtingsput!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-4c526fbf-3aa8-4185-b49f-dc58b4b0d0b8": "spindelschuif!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-c838e9ef-c5b0-45b2-8821-a23a9b4dff73": "nooduitlaat!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-68c04ca9-e61e-484a-b09c-840d3c41834e": "uitmonding!!!",
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-6c7c8ab7-d082-4f23-82a3-195963fb0788": "t-stuk drukriool!!!",
    }
    newVal := mappings[val]
    if newVal != "" {
      val = newVal
    }

    asset.Puttype = val
  case "materiaal":
    val := value.(string)

    mappings := map[string]string{
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-6c7c8ab7-d082-4f23-82a3-195963fb0788": "t-stuk drukriool!!!",
    }
    newVal := mappings[val]
    if newVal != "" {
      val = newVal
    }

    asset.Materiaal = val
  case "afvoertype":
    val := value.(string)

    mappings := map[string]string{
      "https://gitlab.com/fair-data-communities/eudtweg/eudtweg-templates/-/raw/main/definitions/knopen_md.rdf#id-6c7c8ab7-d082-4f23-82a3-195963fb0788": "t-stuk drukriool!!!",
    }
    newVal := mappings[val]
    if newVal != "" {
      val = newVal
    }

    asset.Afvoertype = val
  case "Shape":
    // Shape works differently
    // asset.Shape = string(value)
  case "exterior":
    asset.transientPolygonState = "exterior"
  case "interior":
    asset.transientPolygonState = "interior"
  case "pos":
    posData := value.(string)
    BuildKnoop(asset, posData)
  default:
    fmt.Printf("Unknown key: %v\n", key)
  }
}

func (asset *Knoop) GetValue(key string) interface {} {
  switch key {
  case "Id":
    return asset.Id
  case "ObjectId":
    return asset.ObjectId
  case "objecttype":
    return asset.Objecttype
  case "puttype":
    return asset.Puttype
  case "materiaal":
    return asset.Materiaal
  case "afvoertype":
    return asset.Afvoertype
  default:
    panic(fmt.Sprintf("Unknown field for GetValue: %s", key))
  }
}

//
// Internal or deprecated
//

func BuildKnoop(result *Knoop, pos string) (*Knoop) {
  posFix := strings.Split(pos, " ")
  posValue, _ := strconv.ParseFloat(posFix[1], 32)
  if (posValue > 400000) {
    result.Shape = &gml.GmlShape{
      Points: []*gml.GmlPoint{
        &gml.GmlPoint{
          Id: result.Id + ".pos",
          Pos: pos,
          SrsName: "urn:ogc:def:crs:EPSG::28992",
          SrsDimension: "2",
        },
      },
    }
    return result
  }

  result.Shape = &gml.GmlShape{
    Points: []*gml.GmlPoint{
      &gml.GmlPoint{
        Id: result.Id + ".pos",
        Pos: pos,
        SrsName: "http://www.opengis.net/gml/srs/epsg.xml#4258",
        SrsDimension: "2",
      },
    },
  }
  return result
}

func BuildMultisurface(result *Knoop, pos string) (*Knoop) {
  side := result.transientPolygonState
  if result.Shape.MultiSurface == nil {
    result.Shape = &gml.GmlShape{
      MultiSurface: &gml.GmlMultiSurface{
        Id: result.Id + ".pos",
        SrsName: "urn:ogc:def:crs:EPSG::28992",
        SurfaceMembers: []*gml.GmlSurfaceMember{
          &gml.GmlSurfaceMember{
            Polygon: &gml.GmlPolygon{
              Id: result.Id + ".pos.1",
            },
          },
        },
      },
    }
  }

  polygon := result.Shape.MultiSurface.SurfaceMembers[0].Polygon
  if side == "interior" {
    polygon.Interior = &gml.GmlInterior {
      LinearRing: &gml.GmlLinearRing {
        Pos: pos,
      },
    }
  } else {
    polygon.Exterior = &gml.GmlExterior {
      LinearRing: &gml.GmlLinearRing {
        Pos: pos,
      },
    }
  }


  return result
}

func BuildMulticurve(result *Knoop, pos string, dimension string) (*Knoop) {
  if (result.Shape == nil) || (result.Shape.MultiCurve == nil) {
    result.Shape = &gml.GmlShape{
      MultiCurve: &gml.GmlMultiCurve{
        Id: result.Id + ".ln",
        SrsName: "urn:ogc:def:crs:EPSG::28992",
        SrsDimension: dimension,
        CurveMembers: []*gml.GmlCurveMember{},
      },
    }
  }
  existingMemberCount := len(result.Shape.MultiCurve.CurveMembers)
  newMember := &gml.GmlCurveMember{
    LineString: &gml.GmlLineString{
      Id: result.Id + ".ln." + strconv.Itoa(existingMemberCount),
      SrsDimension: dimension,
      PosList: pos,
    },
  }

  result.Shape.MultiCurve.CurveMembers = append(result.Shape.MultiCurve.CurveMembers, newMember)

  return result
}

func (result *Knoop) AddPointshape(point string) {
  if result.Shape.Points == nil {
    result.Shape.Points = []*gml.GmlPoint{}
  }

  SrsName := "http://www.opengis.net/gml/srs/epsg.xml#4258"
  newPoint := &gml.GmlPoint{
    SrsName: SrsName,
    SrsDimension: "2",
    Pos: point,
  }
  result.Shape.Points = append(result.Shape.Points, newPoint)
}
