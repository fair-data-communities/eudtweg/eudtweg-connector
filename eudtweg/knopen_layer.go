package eudtweg

import(
  "fmt"
  "purplepolarbear.nl/connector/lib/core"
  "purplepolarbear.nl/connector/pipeliner/models"
)

//
// API
//

type KnopenLayer struct {
  name                      string;
  provider                  string;
  consumername              string
  datasetid                 string
  getfeatureproviderfactory models.GetFeatureProviderFactory;
  featureconvertorfactory   models.FeatureConvertorFactory;
}

func NewKnopenLayer(name string, provider string, consumername string, datasetid string, getfeatureproviderfactory models.GetFeatureProviderFactory, featureconvertorfactory models.FeatureConvertorFactory) models.Layer {
  fmt.Printf("*****************\nname: %s\nprovider: %s\nconsumername: %s", name, provider, consumername)
  return &KnopenLayer{
    name: name,
    provider: provider,
    consumername: consumername,
    datasetid: datasetid,
    getfeatureproviderfactory: getfeatureproviderfactory,
    featureconvertorfactory: featureconvertorfactory,
  }
}

func (layer *KnopenLayer) NewItem() models.LayerItem {
  return NewKnoop(layer)
}

// Implementation of Name()
func (layer *KnopenLayer) Name() string {
  return layer.name;
}

// Implementation of DatasetId()
func (layer *KnopenLayer) DatasetId() string {
  return layer.datasetid;
}

// Implementation of Provider()
func (layer *KnopenLayer) Provider() string {
  return layer.provider;
}

func (layer *KnopenLayer) ConsumerName() string {
  return layer.consumername
}

func (layer *KnopenLayer) Namespace() string {
  return "Eudtweg";
}

// Implementation of LayerType()
func (layer *KnopenLayer) LayerType() string {
  return "Knopen";
}

func (layer *KnopenLayer) FullName() (string) {
  return layer.ConsumerName()
}

func (layer *KnopenLayer) FullNamespacedName() (string) {
  return layer.Namespace() + ":" + layer.Name()
}

func (layer *KnopenLayer) FullTypeName() string {
  return core.Snakecase(layer.ConsumerName() + layer.LayerType())
}

func (layer *KnopenLayer) FullNamespacedTypeName() string {
  return core.Snakecase(layer.Namespace() + ":" + layer.ConsumerName() + layer.LayerType())
}

func (layer *KnopenLayer) LayerProperties() *[]*models.LayerProperty {
  return &[]*models.LayerProperty{
    &models.LayerProperty{Name: "Shape", Type: "gml:GeometryPropertyType", Optional: true, Nillable: true},
    &models.LayerProperty{Name: "OBJECTID", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Name", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Provider", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Objecttype", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Puttype", Type: "xsd:string", Optional: true},
  }
}

func (layer *KnopenLayer) GetFeatureProviderFactory() (models.GetFeatureProviderFactory) {
  return layer.getfeatureproviderfactory;
}

func (layer *KnopenLayer) FeatureConvertorFactory() models.FeatureConvertorFactory {
  return layer.featureconvertorfactory;
}

//
// Internal
//
