package eudtweg

import(
  "encoding/xml"
  "fmt"
  "strconv"

  // "purplepolarbear.nl/connector/pipeliner/models"
  "purplepolarbear.nl/connector/lib/gml"
)

//
// API
//

type Streng struct {
  XMLName             xml.Name
  Id                  string `xml:"gml:id,attr"`

  ObjectId            string `xml:"Eudtweg:OBJECTID"`
  Provider            string `xml:"Eudtweg:Provider"`
  Shape               *gml.GmlShape `xml:"Eudtweg:Shape"`
  VanId               string `xml:"Eudtweg:VanId"`
  NaarId              string `xml:"Eudtweg:NaarId"`
  Strengtype          string `xml:"Eudtweg:Strengtype"`
  Afvoertype          string `xml:"Eudtweg:Afvoertype"`
  Breedte             int `xml:"Eudtweg:Breedte"`
  Materiaal           string `xml:"Eudtweg:Materiaal"`

  transientPolygonState       string
}

func NewStreng(layer *StrengenLayer) *Streng {
  return &Streng{
    Provider: layer.Provider(),
    Shape: &gml.GmlShape{},
  }
}

func (asset *Streng) XmlName() *xml.Name {
  return &asset.XMLName
}

func (asset *Streng) SetValue(key string, value interface {}) {
  /*
  fmt.Printf("*************************************\n")
  fmt.Printf("************************************* %s\n", key)
  fmt.Printf("*************************************\n")
  */

  switch key {
  case "Id":
    asset.Id = value.(string)
  case "id":
    asset.ObjectId = value.(string)
  case "van_id":
    asset.VanId = value.(string)
  case "naar_id":
    asset.NaarId = value.(string)
  case "strengtype":
    asset.Strengtype = value.(string)
  case "afvoertype":
    asset.Afvoertype = value.(string)
  case "breedte":
    valueAsString := value.(string)
    val, _ := strconv.Atoi(valueAsString)
    asset.Breedte = val
  case "materiaal":
    asset.Materiaal = value.(string)
  case "Shape":
    // Shape works differently
    // asset.Shape = string(value)
  case "exterior":
    asset.transientPolygonState = "exterior"
  case "interior":
    asset.transientPolygonState = "interior"
  case "pos":
    posData := value.(string)
    BuildMultiline(asset, posData)
  default:
    fmt.Printf("Unknown key: %v\n", key)
  }
}

func (asset *Streng) GetValue(key string) interface {} {
  switch key {
  case "Id":
    return asset.Id
  case "ObjectId":
    return asset.ObjectId
  case "van_id":
    return asset.VanId
  case "naar_id":
    return asset.NaarId
  case "strengtype":
    return asset.Strengtype
  case "afvoertype":
    return asset.Afvoertype
  case "breedte":
    return asset.Breedte
  case "materiaal":
    return asset.Materiaal
  default:
    panic(fmt.Sprintf("Unknown field for GetValue: %s", key))
  }
}

//
// Internal or deprecated
//

func BuildMultiline(result *Streng, pos string) (*Streng) {
  if (result.Shape == nil) || (result.Shape.MultiLineString == nil) {
    result.Shape = &gml.GmlShape{
      MultiLineString: &gml.GmlMultiLineString{
        Id: result.Id + ".ln",
        // SrsName: "urn:ogc:def:crs:EPSG::28992",
        // SrsDimension: "3",
        LineString: []*gml.GmlLineString{},
      },
    }
  }
  existingMemberCount := len(result.Shape.MultiLineString.LineString)
  newMember := &gml.GmlLineString{
    Id: result.Id + ".ln." + strconv.Itoa(existingMemberCount),
    SrsDimension: "2",
    PosList: pos,
  }

  result.Shape.MultiLineString.LineString = append(result.Shape.MultiLineString.LineString, newMember)

  return result
}
