package eudtweg

import(
  "fmt"
  "purplepolarbear.nl/connector/lib/core"
  "purplepolarbear.nl/connector/pipeliner/models"
)

//
// API
//

type StrengenLayer struct {
  name                      string;
  provider                  string;
  consumername              string
  datasetid                 string
  getfeatureproviderfactory models.GetFeatureProviderFactory;
  featureconvertorfactory   models.FeatureConvertorFactory;
}

func NewStrengenLayer(name string, provider string, consumername string, datasetid string, getfeatureproviderfactory models.GetFeatureProviderFactory, featureconvertorfactory models.FeatureConvertorFactory) models.Layer {
  fmt.Printf("*****************\nname: %s\nprovider: %s\nconsumername: %s", name, provider, consumername)
  return &StrengenLayer{
    name: name,
    provider: provider,
    consumername: consumername,
    datasetid: datasetid,
    getfeatureproviderfactory: getfeatureproviderfactory,
    featureconvertorfactory: featureconvertorfactory,
  }
}

func (layer *StrengenLayer) NewItem() models.LayerItem {
  return NewStreng(layer)
}

// Implementation of Name()
func (layer *StrengenLayer) Name() string {
  return layer.name;
}

// Implementation of DatasetId()
func (layer *StrengenLayer) DatasetId() string {
  return layer.datasetid;
}

// Implementation of Provider()
func (layer *StrengenLayer) Provider() string {
  return layer.provider;
}

func (layer *StrengenLayer) ConsumerName() string {
  return layer.consumername
}

func (layer *StrengenLayer) Namespace() string {
  return "Eudtweg";
}

// Implementation of LayerType()
func (layer *StrengenLayer) LayerType() string {
  return "Strengen";
}

func (layer *StrengenLayer) FullName() (string) {
  return layer.ConsumerName()
}

func (layer *StrengenLayer) FullNamespacedName() (string) {
  return layer.Namespace() + ":" + layer.Name()
}

func (layer *StrengenLayer) FullTypeName() string {
  return core.Snakecase(layer.ConsumerName() + layer.LayerType())
}

func (layer *StrengenLayer) FullNamespacedTypeName() string {
  return core.Snakecase(layer.Namespace() + ":" + layer.ConsumerName() + layer.LayerType())
}

func (layer *StrengenLayer) LayerProperties() *[]*models.LayerProperty {
  return &[]*models.LayerProperty{
    &models.LayerProperty{Name: "Shape", Type: "gml:GeometryPropertyType", Optional: true, Nillable: true},
    &models.LayerProperty{Name: "OBJECTID", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "VanId", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "NaarId", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Puttype", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Afvoertype", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Breedte", Type: "xsd:int", Optional: true},
    &models.LayerProperty{Name: "Materiaal", Type: "xsd:string", Optional: true},
  }
}

func (layer *StrengenLayer) GetFeatureProviderFactory() (models.GetFeatureProviderFactory) {
  return layer.getfeatureproviderfactory;
}

func (layer *StrengenLayer) FeatureConvertorFactory() models.FeatureConvertorFactory {
  return layer.featureconvertorfactory;
}

//
// Internal
//
